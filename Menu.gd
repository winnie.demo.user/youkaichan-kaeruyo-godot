extends Control

func _ready():
    $NewGame.grab_focus()
    Game.get_node("TitleMusic").play()

func _on_NewGame_pressed():
    Game.get_node("TitleMusic").stop()
    Game.get_node("GameMusic").play()
    get_tree().change_scene("levels/Level.tscn")


func _on_Quit_pressed():
    get_tree().quit()
