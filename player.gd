extends KinematicBody2D

var vel = Vector2()
var UP = Vector2(0, -1)
var move_speed = 8*15*8

var gravity
var max_jump_velocity
var min_jump_velocity
var max_jump_height = 4.5 * Game.TILESIZE
var min_jump_height = 1 * Game.TILESIZE
var jump_duration = 0.3

var on_floor

func _ready():
    gravity = 2*max_jump_height / pow(jump_duration, 2)
    max_jump_velocity = -sqrt(2*gravity*max_jump_height)
    min_jump_velocity = -sqrt(2*gravity*min_jump_height)
    
    
func _input(event):
    if event.is_action_pressed("btn_up") and on_floor:
        vel.y = max_jump_velocity
        on_floor = false
        $Jump.pitch_scale = rand_range(0.5, 1.5)
        $Jump.play()
    if event.is_action_released("btn_up") && vel.y < -100:
        vel.y = -100

func _process(delta):
    _get_input()
    vel.y += gravity*delta
    vel = move_and_slide(vel, UP)
    
    
    if is_on_floor():
        on_floor = true
        
    if on_floor:
        $Body.scale.y = 1
    else:
        $Body.scale.y = 1.5
    
func _get_input():
    var move_direction = int(Input.is_action_pressed("btn_right")) - int(Input.is_action_pressed("btn_left"))
    vel.x = lerp(vel.x, move_speed * move_direction, _get_horizontal_w())
    if move_direction != 0:
        $AnimationPlayer.play("walk")
        $Body.scale.x = -move_direction
    else:
        $AnimationPlayer.play("idle")
        
func _get_horizontal_w():
    if on_floor:
        return 0.5
    else:
        return 0.5